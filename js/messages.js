/**
 * Created by Lyze on 03.05.2016.
 */

function appendMessageObject(message) {
    appendMessageInternal(message.author.username, message.author.avatarURL, message.content);
}

function appendMessageInternal(username, avatarurl, message) {
    if (message == null) {
        message = "<Error>";
    }

    console.log("Appending message:" + username + "/" + message);

    $('#messages').append('<li class="collection-item avatar">' +
                        '<img src="' + avatarurl + '" alt="" class="circle">' +
                        '<span class="title">' + username + '</span>' +
                        '<p>' +
                            message.replace(/(?:\r\n|\r|\n)/g, '<br />') +
                        '</p>' +
                    '</li>'
    );

    if ($('#messages').find('li').size() > 50) {
        $('#messages').find('li:nth-child(1)').remove();
    }

    scrollDown();
}

function scrollDown() {
    $("#messages").animate({ scrollTop: $('#messages').prop("scrollHeight")}, 0);
}

function clearMessages() {
    $('#messages').find('li').remove();
}

function changeChannel(channelId, type) {
    unhighlightChat(channelId);
    if (type == "private") {
        var chan = bot.privateChannels.get("id", channelId);
        console.log(chan);
        console.log("Changing private channel to " + chan.recipient.username);

        changeChannelHeaderName(chan.recipient.username);
        changeChannelHeaderId(chan.id);
        changeChannelHeaderType("private");
        clearMessages();

        loadMessages(chan.id);

    } else {
        var chan = bot.channels.get("id", channelId);

        console.log("Changing channel to " + chan.name);

        changeChannelHeaderName(chan.server.name + "#" + chan.name);
        changeChannelHeaderId(channelId);
        changeChannelHeaderType("public");
        clearMessages();

        loadMessages(chan.id);
    }
}

function loadMessages(channelId) {

    var par1 = channelId;
    if (getChannelHeaderType() == "private") {
        par1 = bot.privateChannels.get('id', channelId);
    }

    bot.getChannelLogs(par1, function (error, messages) {
        for (var i = messages.length - 1; i >= 0; i--) {
            appendMessageObject(messages[i]);
        }

        scrollDown();
    });
}

function changeChannelHeaderName(name) {
    $('#channelname').text(name);
}

function changeChannelHeaderType(type) {
    $('#channeltype').text(type);
}

function getChannelHeaderType() {
    return $('#channeltype').text();
}


function changeChannelHeaderId(id) {
    $('#channelid').text(id);
}

function getChannelHeaderId() {
    return $('#channelid').text();
}

function sendMessage() {
    console.log("sending message" + $('#messageInput').val());

    var par1 = getChannelHeaderId();
    if (getChannelHeaderType() == "private") {
        par1 = bot.privateChannels.get('id', getChannelHeaderId());
    }

    console.log("Sending message to: " + par1);

    bot.sendMessage(par1, $('#messageInput').val());

    $('#messageInput').val("");
}