/**
 * Created by Lyze on 03.05.2016.
 */

//var Discord = require("discord.min.7.0.1.js");
var bot = new Discord.Client();
var unreadMessage = false;

setInterval(function() {
    if (unreadMessage) {
        if ($('nav').hasClass('red')) {
            document.title = "Unread Message!";
            $('nav').addClass('teal').removeClass('red');
        } else {
            document.title = "New Unread Message!";
            $('nav').addClass('red').removeClass('teal');
        }
    } else {
        if ($('nav').hasClass('red')) {
            $('nav').addClass('teal').removeClass('red');
        }
        if (document.title != "Discord Client")
            document.title = "Discord Client";
    }
}, 1000);

bot.on("message", function(message) {
    if (message.channel.id == getChannelHeaderId())
        appendMessageObject(message);
    else {
        unreadMessage = true;
        highlightChat(message.channel.id);
    }
});


function highlightChat(id) {
    console.log("Highlighting: " + id);
    $('.channel').each(function() {
        if ($(this).find('a').attr('onclick').indexOf(id) > -1) {
            $(this).find('a').addClass('red-text');
        }
    });
}

function unhighlightChat(id) {
    console.log("Unhighlighting: " + id);

    var highlighted = 0;
    $('.channel').each(function() {
        if ($(this).find('a').hasClass('red-text')) {
            highlighted++;
        }
        if ($(this).find('a').attr('onclick').indexOf(id) > -1) {
            $(this).find('a').removeClass('red-text');
            highlighted--;
        }
    });

    unreadMessage = highlighted > 0;
}


bot.on("ready", function() {
    console.log("ready to begin with everything");
    $('#loginModal').closeModal();
    $('#loginProgressBar').toggle(false);

    populateNavigation();
    changeChannel(bot.channels[0].id, "public");
});

function login() {
    bot.login($('#loginUsername').val(), $('#loginPassword').val()).then(onSuccess).catch(onError);
    $('#loginProgressBar').toggle(true);
}


function onError() {
    $('#loginProgressBar').toggle(false);
}

function onSuccess() {
    console.log("successfully logged in");
}

function populateNavigation() {
    var ul = $('#slide-out');
    ul.empty();

    console.log("Client is in " + bot.servers.length + " servers");

    console.log("Appending " + bot.channels.length + " channels to nav");
    console.log("Appending " + bot.privateChannels.length + " private channels to nav");

    for (var i = 0; i < bot.servers.length; i++) {

        var channels = "";
        for (var j = 0; j < bot.servers[i].channels.length; j++) {
            if (bot.servers[i].channels[j].type == "text")
                channels += '<li class="publicChannel channel"><a onclick="changeChannel(\'' + bot.servers[i].channels[j].id + '\', \'public\')">' + bot.servers[i].channels[j].name + '</a></li>\n';
        }

        ul.append(
            '<li class="no-padding">' +
            '	<ul class="collapsible collapsible-accordion"> ' +
            '		<li>' +
            '			<a class="collapsible-header">' + '<div class="chip truncate"><img src="' + bot.servers[i].iconURL + '" />' + bot.servers[i].name + '</div>' + '</a>' +
            '			<div class="collapsible-body">' +
            '				<ul>' +
                               channels +
            '				</ul>' +
            '			</div>' +
            '		</li>' +
            '	</ul>' +
            '</li>'
        );
    }

    channels = "";

    for (i = 0; i < bot.privateChannels.length; i++) {
        channels += '<li class="privateChannel channel"><a onclick="changeChannel(\'' + bot.privateChannels[i].id + '\', \'private\')">' + bot.privateChannels[i].recipient.username + '</a></li>\n';
    }

    ul.append(
        '<li class="no-padding">' +
        '	<ul class="collapsible collapsible-accordion"> ' +
        '		<li>' +
        '			<a class="collapsible-header">' + '<div class="chip truncate"><img src="' + 'images/private.jpg' + '" />' + "Private Channels" + '</div>' + '</a>' +
        '			<div class="collapsible-body">' +
        '				<ul>' +
                            channels +
        '				</ul>' +
        '			</div>' +
        '		</li>' +
        '	</ul>' +
        '</li>'
    );

    // Initialize them
    $('.collapsible').collapsible({
        accordion : false
    });
}
