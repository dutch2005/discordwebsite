/**
 * Created by Lyze on 03.05.2016.
 */
$('document').ready(function() {
    initMaterialize();

    $('#loginPassword').keyup(function (event) {
        if (event.keyCode == 13) {
            document.getElementById("loginSubmit").click();
        }
    });
    $('#loginUsername').keyup(function (event) {
        if (event.keyCode == 13) {
            document.getElementById("loginSubmit").click();
        }
    });

    $('#loginModal').openModal({
        dismissible: 0
    });
});

function initMaterialize() {
    $(".button-collapse").sideNav();
    $(".dropdown-button").dropdown();
    $('.collapsible').collapsible({
        accordion : false
    });
}