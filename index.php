<!DOCTYPE html>
<html>
<head>

    <script type="text/javascript">
        if (typeof console == "undefined") {
            window.console = {
                log: function () {}
            };
        }
    </script>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <!-- Discord Thingy -->
    <script type="text/javascript" src="https://hydrabolt.github.io/discord.js/js/discord.min.6.0.0.js"></script>

    <!-- Own Stuff -->
    <link type="text/css" rel="stylesheet" href="css/main.css" />
    <script type="text/javascript" src="js/bot.js"></script>
    <script type="text/javascript" src="js/messages.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <title>Discord Client</title>
</head>
<body>
<span class="invisible" id="channelid">#Id</span><span class="invisible" id="channeltype"></span>
    <header>
        <nav class="teal lighten-2">
            <a href="#" class="brand-logo truncate"><span id="channelname" class="flow-text" >#Name</span></a>
            <ul id="slide-out" class="side-nav fixed">
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
        </nav>
    </header>



    <main>
        <div class="row">
            <div class="col s12">
                <ul id="messages" class="collection">
                </ul>
            </div>
        </div>
        <form action="javascript:;" onsubmit="sendMessage()" id="messageForm">
            <div class="row">
                <div class="col s12">
                    <div class="input-field col s12">
                        <input id="messageInput" type="text" name="message">
                        <label for="messageInput">Message</label>
                    </div>
                </div>
            </div>
        </form>
    </main>




    <div id="loginModal" class="modal">
        <form method="post">
            <div class="modal-content">
                <h4>Login</h4>
                <div class="divider"></div>
                <div class="row">
                    <div class="col s12 m6">
                        <div class="input-field col s12">
                            <input id="loginUsername" type="text" name="username">
                            <label for="loginUsername">Username</label>
                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div class="input-field col s12">
                            <input id="loginPassword" type="password" name="password">
                            <label for="loginPassword">Password</label>
                        </div>
                    </div>
                </div>
                <div id="loginProgressBar" class="progress invisible">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a onclick="login();" id="loginSubmit" class="modal-action waves-effect waves-green btn">Submit</a>
            </div>
        </form>
    </div>

</body>
</html>
